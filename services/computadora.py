from models.computadora import Computadora as ComputadoraModel
from schemas.computadora import Computadora

class ComputadoraService():
    
    def __init__(self, db) -> None:
        self.db = db
    
    def get_computadoras(self):
        result = self.db.query(ComputadoraModel).all()
        return result
    
    def get_computadora_by_id(self, id: int):
        result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
        return result
    
    def get_computadoras_by_marca(self, marca: str):
        result = self.db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
        return result
    
    def create_computadora(self, computadora: Computadora):
        new_computadora = ComputadoraModel(**computadora.model_dump())
        self.db.add(new_computadora)
        self.db.commit()
        return
    
    def delete_computadora(self, id: int):
        result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
        self.db.delete(result)
        self.db.commit()
    
    def update_computadora(self, id: int, computadora: Computadora):
        result = self.db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
        result.marca = computadora.marca
        result.modelo = computadora.modelo
        result.color = computadora.color
        result.ram = computadora.ram
        result.almacenamiento = computadora.almacenamiento
        self.db.commit()
        return 
