from fastapi import Path, Query, Depends, APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from models.computadora import Computadora as ComputadoraModel
from fastapi.encoders import jsonable_encoder
from config.database import Session
from middlewares.jwt_bearer import JWTBearer
from services.computadora import ComputadoraService
from schemas.computadora import Computadora

#Creamos una instancia de APIRouter 
computadora_router = APIRouter()

#MOSTRAR LISTA DE COMPUTADORAS
@computadora_router.get('/computadoras', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def get_computadoras() -> List[Computadora]:
    db = Session()
    #result = db.query(ComputadoraModel).all()
    result = ComputadoraService(db).get_computadoras()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#MOSTRAR COMPUTADORA POR ID
@computadora_router.get('/computadoras/{id}', tags=['computadoras'], response_model=Computadora, status_code=200)
def get_computadora_by_id(id: int) -> Computadora:
    db = Session()
    #result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    result = ComputadoraService(db).get_computadora_by_id(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Not found"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#MOSTRAR LISTA DE COMPUTADORAS POR MARCA
@computadora_router.get('/computadoras/', tags=['computadoras'], response_model=Computadora, status_code=200)
def get_computadoras_by_marca(marca: str) -> List[Computadora]:
    db = Session()
    #result = db.query(ComputadoraModel).filter(ComputadoraModel.marca == marca).all()
    result = ComputadoraService(db).get_computadoras_by_marca(marca)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#CREAR COMPUTADORA
@computadora_router.post('/computadoras', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def create_computadora(computadora: Computadora) -> dict:
    db = Session()
    # Utilzamos el modelo y le pasamos la información que vamos a registrar. Los asteriscos son para pasar los parametros en el diccionario
    # new_computadora = ComputadoraModel(**computadora.dict())
    #new_computadora = ComputadoraModel(**computadora.model_dump())
    # Ahora a la BD añadimos esa Computadora
    # db.add(new_computadora)
    # Guardamos los datos
    #db.commit()
    ComputadoraService(db).create_computadora(computadora)
    return JSONResponse(content={"message": "Se ha registrado la computadora"})

#BORRAR COMPUTADORA
@computadora_router.delete('/computadoras/{id}', tags=['computadoras'], response_model=List[Computadora], status_code=200)
def delete_computadora(id: int) -> dict:
    db = Session()
    #result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    result = ComputadoraService(db).get_computadora_by_id(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    #db.delete(result)
    #db.commit()
    ComputadoraService(db).delete_computadora(id)
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})

#EDITAR COMPUTADORA
@computadora_router.put('/computadoras/{id}', tags=['computadoras'], response_model=Computadora, status_code=200)
def update_computadora(id: int, computadora: Computadora) -> dict:
    db = Session()
    #result = db.query(ComputadoraModel).filter(ComputadoraModel.id == id).first()
    result = ComputadoraService(db).get_computadora_by_id(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "No se ha encontrado la computadora"})
    #result.marca = computadora.marca
    #result.modelo = computadora.modelo
    #result.color = computadora.color
    #result.ram = computadora.ram
    #result.almacenamiento = computadora.almacenamiento
    #db.commit()
    ComputadoraService(db).update_computadora(id, computadora)
    return JSONResponse(status_code=200, content={"message": "Se ha actualizado la computadora"})
