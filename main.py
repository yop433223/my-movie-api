from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from config.database import engine, Base, Session
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from models.computadora import Computadora as ComputadorasModel
from fastapi import Depends
from middlewares.jwt_bearer import JWTBearer
from typing import List, Optional
from routers.user import user_router
from routers.computadora import computadora_router

app = FastAPI()
app.title = "Venta de Computadoras y Películas :)"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)
app.include_router(computadora_router)

Base.metadata.create_all(bind=engine)

@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Venta de Computadoras y Películas :)</h1>')

